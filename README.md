# python

## set python to python3
1. cd ~
2. gedit .bash_aliases
3. alias python='/usr/bin/python3'

## How to find the site packages installed 
1. python -c "import site; print(site.getsitepackages())"
2. python -c "import site; print(site.getsitepackages())"
3. python -m site --user-site
4. pip list --user
5. pip freeze --user

## remove pip and reinstall 
1. sudo apt-get purge python-pip
2. sudo apt-get update
3. sudo apt-get install python-pip

## Create a virtual environment
1. Install  virtual environment
   sudo pip install virtualenv
   virtualenv --version
2. Make a Virtual Environment with Python 3.6 ...
   virtualenv py3
3. Activate your virtual environemnt ...
   source py3/bin/activate

## Create a virtual environment (from scratch)

### Install pip first
sudo apt-get install python3-pip

### Then install virtualenv using pip3
sudo pip3 install virtualenv 
### Now create a virtual environment
virtualenv venv 

you can use any name insted of venv
You can also use a Python interpreter of your choice

virtualenv -p /usr/bin/python2.7 venv
### Active your virtual environment:
source venv/bin/activate
### Using fish shell:
source venv/bin/activate.fish
### To deactivate:
deactivate
Create virtualenv using Python3
virtualenv -p python3 myenv
### Instead of using virtualenv you can use this command in Python3
python3 -m venv myenv

###  installing pip

python -m pip install --upgrade pip

# Jupyter

## Make Jupyter cell wider

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>"))

## Make Jupyter autocomplete code
conda install -c conda-forge jupyter_nbextensions_configurator
jupyter nbextension enable hinterland/hinterland


# Install python 3.7 from scratch

(https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/)

1. Start by updating the packages list and installing the prerequisites:

sudo apt update
sudo apt install software-properties-common

2. Next, add the deadsnakes PPA to your sources list:

  sudo add-apt-repository ppa:deadsnakes/ppa

3. Once the repository is enabled, install Python 3.7 with:

sudo apt install python3.7

4. python3.7 --version

