# Python Install from scratch:

(https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/)

1. Start by updating the packages list and installing the prerequisites:

sudo apt update
sudo apt install software-properties-common

2. Next, add the deadsnakes PPA to your sources list:

sudo add-apt-repository ppa:deadsnakes/ppa


3. Once the repository is enabled, install Python 3.7 with:

sudo apt install python3.7

4. At this point, Python 3.7 is installed on your Ubuntu system and ready to be used. You can verify it by typing:

python3.7 --version


Ubuntu 18.04 ships with Python 3, as the default Python installation. Complete the following steps to install pip (pip3) for Python 3:

1. Start by updating the package list using the following command:

sudo apt update
sudo apt install python3-pip
pip3 --version
The version number may vary, but it will look something like this:
pip 9.0.1 from /usr/lib/python3/dist-packages (python 3.6)



Install packages

Installing Packages with Pip using the Requirements Files
requirement.txt is a text file that contains a list of pip packages with their versions which are required to run a specific Python project.

Use the following command to install a list of requirements specified in a file:


pip3 install -r requirements.txt
Copy
Listing Installed Packages
To list all the installed pip packages use the command below:

pip3 list
Copy
Upgrade a Package With Pip
To upgrade an already installed package to the latest version use the following command:

pip3 install --upgrade package_name
Copy
Uninstalling Packages With Pip
To uninstall a package run:

pip3 uninstall package_name
Copy
Conclusion
In this tutorial, you have learned how to install pip on your Ubuntu machine and how to manage Python packages using pip. For more information about pip, visit the pip user guide page.

If you have any questions or feedback, feel free to comment below.

python
pip
ubuntu

report this ad
RELATED TUTORIALS

How to deploy Odoo 12 on Ubuntu 18.04
How to deploy Odoo 11 on Ubuntu 18.04
How to install Odoo 11 on Ubuntu 16.04
How to Install Flask on Ubuntu 18.04
Install Odoo 12 on CentOS 7
How to Install Django on Ubuntu 18.04
How to Install PyCharm on Ubuntu 18.04
How to Create Python Virtual Environments on Ubuntu 18.04
How to install OpenCV on Ubuntu 18.04
How to install Pip on Debian 9

